<?php include "connection.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="go_back">
<a href="index.php">
<img src="./src/images/go-back.png" alt="" style="height: 50px">
Go back
</a>
<h1>DELETE AN ITEM FROM THE TABLE</h1>
<form action= "./index.php" method="post">
        Delete the following task? : <?php 
        
        $id = $_GET["id"];
        $sql = "SELECT task, task_difficult, assigned_to FROM list_of_todo WHERE id= $id";
        $result = $mysqli->query($sql);
        // Show results of data
        if ($result->num_rows > 0) {
        // Show tasks in a table
            echo "<table>";
                    echo 
                    "
                    <th>Task</th>
                    <th>Difficulty</th>
                    <th>Assigned to</th>
                    ";
                while($row = $result->fetch_assoc()) {
                    echo 
                    "<tr>" .
                    "<td>" .    $row{'task'} . "</td>" .
                    "<td>".     $row{'task_difficult'} . "</td>" .
                    "<td>" .    $row{'assigned_to'}. "</td>" .
                    "</tr>"
                    ;
                }
            echo "</table>";
        } 
        ?> 
        <br>
        <br>
        <input type="submit" name="delete" value="Delete"/> 
</form>

<?php
// DELETE FROM DATABASE
$delete_task  = !empty ($_REQUEST["delete"]) ? $_REQUEST["delete"] : NULL;

if(!empty($delete_task)){
    $sql4 = $mysqli->prepare("DELETE FROM list_of_todo WHERE id= ?");
    $sql4->bind_param('s', $id);
    $sql4->execute();
}
?>
</body>
</html>